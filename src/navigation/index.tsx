/**
 * If you are not familiar with React Navigation, check out the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';

import * as LoginRoutes from '../containers/login/routes';
import * as homeRoutes from '../containers/dashboard/routes';

import { RootStackParamList } from '../../types';
import LinkingConfiguration from './LinkingConfiguration';
import { Routes } from '../interfaces/dashboard';

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
        <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();

const setRoutes = () => {
  const routes = [
    ...LoginRoutes.routes,
    ...homeRoutes.routes
  ];

  return routes.map((cm: Routes, i) => (
    <Stack.Screen key={`route-${i}`} name={cm.path} component={cm.component} options={cm.options} />
  ));
}

function RootNavigator() {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      { setRoutes() }
    </Stack.Navigator>
  );
}
