import * as actionsTypes from './types';


export const setLanguage = (params: any) => ({
  type: actionsTypes.SET_LANGUAGE,
  payload: params,
});

export const setMobile = (params: any) => ({
  type: actionsTypes.SET_MOBILE,
  payload: params,
});

export const setTestData = () => ({
  type: actionsTypes.TEST_DATA
});
