export const SET_LANGUAGE = 'app/SET_LANGUAGE';
export const SET_MOBILE = 'app/SET_MOBILE';
export const TEST_DATA = 'dashboard/TEST_DATA';
export const SET_TEST_DATA = 'dashboard/SET_TEST_DATA';
export const SET_LOADING = 'dashboard/SET_LOADING';
