import * as actionsTypes from './types';
import produce from 'immer';


const INITIAL_STATE = {
  language: 'en',
  mobile: 0,
  test_data: null,
  loadings: {
    testDataLoading: false
  }
};


const setLoading = (draft: any, action: any) => {
  const { prop, value } = action.payload;
  const loadings = { ...draft.loadings };
  loadings[prop] = value;
  draft.loadings = loadings;
};


export default produce((draft, action) => {
  switch (action.type) {
    case actionsTypes.SET_TEST_DATA:
      draft.test_data = action.payload;
      break;
    case actionsTypes.SET_LANGUAGE:
      draft.language = action.payload;
      break;
    case actionsTypes.SET_MOBILE:
      draft.mobile = action.payload;
      break;
    case actionsTypes.SET_LOADING:
      setLoading(draft, action);
      break;
    default:
      break;
  }
}, INITIAL_STATE);

