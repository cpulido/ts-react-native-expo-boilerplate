import * as React from 'react';
import { connect } from 'react-redux';
import { Text, View } from '../../../../../components/Themed';
import AutoForm from '../../../../../components/AutoForm';
import Card from '../../../../../components/Card';
import LayoutContainer from '../../../layout/index';
import { Button } from 'react-native-elements';
import { Input } from '../../../../../components/AutoForm/interface';
import { ON_PRESS_LEFT_ICON_HEADER, ON_PRESS_RIGHT_ICON_HEADER } from '../../../../../constants';
import eventManager from '../../../../../utils/eventManager';
import formatter from '../../../../../utils/formatter';
import { isEqual } from 'lodash';
import { Helmet } from '../../../layout/interface';

const styles = {
  containerLogin: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#dee3e0',
    padding: 20
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  card: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 2,
    shadowOpacity: 0.16,
    elevation: 8,
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 5
  }
}

const helmet: Helmet = {
  iconLeft: 'arrow-back-outline',
  iconRight: 'search-outline',
  title: 'Dashboard',
};

class HomeViewContainer extends React.Component<any, any>{  
  constructor(props: any) {
    super(props);

    this.state = {
      formInputs: {}
    };
    // @ts-ignore
    this.AutoForm = React.createRef();
  }

  getInputs() {
    const inputs: Array<Input> = [
      {
        name: 'usuario',
        type: 'text',
        max: 10,
        label: 'USUARIO',
        secureTextEntry: false,
        placeholder: "HOLA TEST",
        errorMessage: 'por favor coloque usuario'

      },
      {
        name: 'numeros',
        type: 'number',
        max: 10,
        leftIcon: {
          type: 'font-awesome',
          name: 'user' 
        },
        label: 'Numeros',
        // containerStyle: styles.containerLogin,
        placeholder: "HOLA TEST",
        errorMessage: 'Contraseña incorrecta'
      },
      {
        name: 'textprueba',
        type: 'text',
        max: 10,
        label: 'HOLA LABEL 3',
        // containerStyle: styles.card,
        placeholder: "HOLA TEST"
      }
    ];

  
    return inputs;
  }

  componentDidMount() {
    this.onPressedHeaderButtons();
  }

  shouldComponentUpdate(nextProps: any, nextState: any) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentWillUnmount() {
    // @ts-ignore
    eventManager.unsubscribe(ON_PRESS_LEFT_ICON_HEADER, this.callbackLeftHeader);
     // @ts-ignore
    eventManager.unsubscribe(ON_PRESS_RIGHT_ICON_HEADER, this.callbackRightHeader);
  }

  onPressedHeaderButtons() {
    // @ts-ignore
    this.callbackLeftHeader = eventManager.on(ON_PRESS_LEFT_ICON_HEADER, () => {
      console.log('left :>> ');
    });

    // @ts-ignore
    this.callbackRightHeader = eventManager.on(ON_PRESS_RIGHT_ICON_HEADER, () => {
      console.log('right :>> ');
    });
  }


  render() {
    return (
      <View>
        <Card>
          <Text>BIENVENDO AL DASHBOARD</Text>
        </Card>
        <Card withoutShadowBorder>
          <AutoForm
            ref={ref => { this.AutoForm = ref; }}
            inputs={this.getInputs()}
            onChange={(data: any) => {
              this.setState({ formInputs: data });
            }}
          />

          <Button
            style={{ width: 50, top:20 }}
            title="VER DATOS"
            type="outline"
            onPress={() => {
              const data = this.AutoForm.getData();
            }}
          />
        </Card>

      </View>
    );
  }
}

const mapStateToProps = (state: any) => {
  const loadings = state.login.loginLoading.value;
  return { loadings };
};

const actions: object = {}

export default LayoutContainer(connect(mapStateToProps, actions)(HomeViewContainer), helmet);
