import { isEqual } from 'lodash';
import * as React from 'react';
import { Icon } from 'react-native-elements';
import Header from '../../../../components/Header';
import { ON_PRESS_LEFT_ICON_HEADER, ON_PRESS_RIGHT_ICON_HEADER } from '../../../../constants';
import eventManager from '../../../../utils/eventManager';
import formatter from '../../../../utils/formatter';

export default class HeaderContainer extends React.Component<any, any>{
  constructor(props: any) {
      super(props);

      this.state = {
          user: '',
          password: '',
      };
    }

    shouldComponentUpdate(nextProps: any, nextState: any) {
      const { clean } = formatter;
      return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
    }

    render() {
      return (
          <Header
            title={this.props.helmet.title}
            color="white"
            leftContainer={ // grid-outline
              <Icon
                name={this.props.helmet.iconLeft}
                type='ionicon'
                color='#000'
                onPress={() => {
                    eventManager.emit(ON_PRESS_LEFT_ICON_HEADER);
                }}
              />
            }
            rightContainer={
              <Icon
                name={this.props.helmet.iconRight}
                type='ionicon'
                color='#000'
                onPress={() => {
                    eventManager.emit(ON_PRESS_RIGHT_ICON_HEADER);
                }}
              />
            }
            />
        );
    }
  };