import * as React from 'react';
import {  View } from '../../../components/Themed';
import Header from './Header';
import { Helmet } from './interface';

export default function withDataFetching(WrappedComponent: Function, helmet: Helmet) {
return class LayoutContainer extends React.Component<any, any>{
  constructor(props: any) {
      super(props);
    }

    render() {
      return [
          <Header key="Helmet" helmet={helmet} />,
          <View key="Body" style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 35, flex: 1 }}>
            <View style={{ flexDirection: 'column', flex: 1, paddingLeft: 20, paddingRight: 20 }}>
              <WrappedComponent {...this.props} />
            </View>
          </View>
      ];
    }
  }
}
