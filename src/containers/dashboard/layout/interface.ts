export interface Helmet {
    iconLeft: string,
    iconRight?: string,
    title: string,
}