import { Routes } from '../../interfaces/dashboard';
import LoginScreen from './views/screens/homeView/index';
 
const routes: Array<Routes> = [
    {
        path: 'home',
        component: LoginScreen
    },
];

export { routes };