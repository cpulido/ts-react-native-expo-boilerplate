import axios from 'axios';
const API_URL = '';

export function callTestService() {
  return () => axios({
    method: 'get',
    url: 'https://api.scaleserp.com/search?api_key=demo&location=New+York%2CNew+York%2CUnited+States&q=bitcoin',
    headers: {
      'Content-Type': 'application/json',
      // 'Authorization': 'Token'
    },
    data: { /* data body */ }
  });
}
