import { takeLatest, all } from 'redux-saga/effects';
import * as actionsTypes from '../store/types';
import * as workers from './workers';


export default function* watcherSaga() {
  yield all([
    takeLatest(actionsTypes.SET_LANGUAGE, workers.testFunctionSaga),
    takeLatest(actionsTypes.TEST_DATA, workers.testFunctionSaga)
  ]);
}
