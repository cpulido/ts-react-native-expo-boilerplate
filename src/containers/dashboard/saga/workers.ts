import { call, put, select } from 'redux-saga/effects';
import * as actionsTypes from '../store/types';
import { callTestService } from './calls';
import { httpBodyResponse } from '../../../interfaces/http';

export function* testFunctionSaga() {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'testDataLoading', value: true } });

    const request = callTestService();
    const response: httpBodyResponse = yield call(request);
    const serverResponse = response.data;
    if (serverResponse.request_info.success) {
      yield put({ type: actionsTypes.SET_TEST_DATA, payload: serverResponse });
      yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'testDataLoading', value: false } });

    }
  } catch (error) {
     yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'testDataLoading', value: false } });
  }
}
