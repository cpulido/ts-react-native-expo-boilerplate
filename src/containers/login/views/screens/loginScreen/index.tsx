import * as React from 'react';
import { Image } from 'react-native';
import { Input, Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { View } from '../../../../../components/Themed';
import { AuthData, Props, reducers, State } from '../../../interfaces/login';
import { login } from '../../../store/action';
import { isEqual } from 'lodash';

import logoImage from '../../../../../assets/images/logo_presico.png';

const styles = {
  containerLogin: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#dee3e0',
    padding: 20
  },
  buttonContainer: {
    flexDirection: 'row',
  }
}

class AuthUserScreen extends React.Component<Props, State>{
  constructor(props: Props) {
    super(props);

    this.state = {
        user: '',
        password: '',
    };
  }


  componentDidUpdate(prevProps: Props, prevState: State) {
    if (this.props.isAuth && !isEqual(prevProps.isAuth, this.props.isAuth)) {
      this.props.navigation.navigate('home');
    }
  }

  onChangeUser(text: string) {
    this.setState({ user: text });
  }


  onChangePassword(text: string) {
    this.setState({ password: text });
  }

  
  onPressLogin() {
    const loginParams: AuthData = {
      user: this.state.user,
      password: this.state.password
    }
    this.props.login(loginParams);
  }

  
  render() {
    const logoImg: any = logoImage;
    
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 100, flex: 1 }}>

          <View style={{ flexDirection: 'column', flex: 1, paddingLeft: 20, paddingRight: 20 }}>
            <Image
              source={logoImg}
              style={{ width: 350, height: 150, marginBottom: 50 }}
            />
            
            <View style={styles.containerLogin}>
              <Input
                placeholder="user"
                onChangeText={this.onChangeUser.bind(this)}
                leftIcon={{ type: 'font-awesome', name: 'user' }}
              />

              <Input
                placeholder="Password"
                onChangeText={this.onChangePassword.bind(this)}
                leftIcon={{ type: 'font-awesome', name: 'lock' }}
                secureTextEntry={true}
              />

              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                  <Button
                    style={{ width: 50 }}
                    title="ENTRAR"
                    type="outline"
                    loading={!!this.props.loadings}
                    onPress={this.onPressLogin.bind(this)}
                  />
              </View>
            </View>
          </View>
        </View>
    );
  }
}

const mapStateToProps = (state: any) => {
  const loadings = state.login.loginLoading.value;
  const isAuth = state.login.isAuth.value;

  return { loadings, isAuth };
};

const actions: object = {
  login
}

export default connect(mapStateToProps, actions)(AuthUserScreen);
