export interface Props {
    navigation: Function,
    loadings: Loadings,
    isAuth: Boolean,
    login: Function,
    loginLoading: Boolean
}

export interface reducers {
    login: loginReducers
}

export interface State {
    user: string,
    password: string
}

export interface AuthData {
    user: string,
    password: string
}

export interface userLogin {
    payload: AuthData
}


interface Loadings {
    login: Boolean
}

interface loginReducers {
    loadings: Loadings,
    loginLoading: Boolean
}
