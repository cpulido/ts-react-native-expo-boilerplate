import { AuthData } from "./login";
import * as actionsTypes from '../store/types';

export interface AuthUserSaga { type: typeof actionsTypes.LOGIN_USER, payload: AuthData }