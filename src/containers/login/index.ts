import reducer from './store/reducer';
import saga from './saga';


const container = {
  reducer,
  saga
};

export default container;
