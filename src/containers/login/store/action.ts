import { AuthData } from '../interfaces/login';
import * as actionsTypes from './types';


export const setLanguage = (params: any) => ({
  type: actionsTypes.SET_LANGUAGE,
  payload: params,
});

export const setMobile = (params: any) => ({
  type: actionsTypes.SET_MOBILE,
  payload: params,
});

export const login = (params: AuthData) => ({
  type: actionsTypes.LOGIN_USER,
  payload: params,
});