export const SET_LANGUAGE = 'app/SET_LANGUAGE';
export const SET_MOBILE = 'app/SET_MOBILE';
export const SET_LOADING = 'app/SET_LOADING';
export const LOGIN_USER = 'app/LOGIN_USER';
export const SET_AUTH = 'app/SET_AUTH';
