import produce, { Immer } from 'immer';
import * as actionsTypes from './types';


const INITIAL_STATE = {
  language: 'en',
  isAuth: false,
  loadings: {
    login: false,
  },
  loginLoading: false
};

export default produce((draft, action) => {
  switch (action.type) {
    case actionsTypes.SET_LANGUAGE:
      draft.language = action.payload;
      break;
    case actionsTypes.SET_LOADING:
      draft.language = action.payload;
      break;
      case actionsTypes.SET_AUTH:
        draft.isAuth = action.payload;
        break;
    default:
      break;
  }
}, INITIAL_STATE);