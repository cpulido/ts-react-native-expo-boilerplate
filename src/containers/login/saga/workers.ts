import { call, put, select } from 'redux-saga/effects';
import * as actionsTypes from '../store/types';
import { loginUser } from './calls';
import { httpBodyResponse } from '../../../interfaces/http';
import { AuthUserSaga } from '../interfaces/saga';
import { ToastAndroid } from 'react-native';

export function* login(action: AuthUserSaga) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'loginLoading', value: true } });
    
    if (action.payload.user.length && action.payload.password.length) {
      const request = loginUser(action.payload);
      const response: httpBodyResponse = yield call(request);
      const serverResponse = response.data;
      if (serverResponse.request_info.success) {
        yield put({ type: actionsTypes.SET_AUTH, payload: { prop: 'isAuth', value: true } });
        yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'loginLoading', value: false } });
      }
    } else {
      ToastAndroid.showWithGravity(
        'No deje campos vacíos',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM
      );
      yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'loginLoading', value: false } });

    }
  } catch (error) {
    ToastAndroid.showWithGravity(
      error.message,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM
    );
     yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'loginLoading', value: false } });
  }
}
