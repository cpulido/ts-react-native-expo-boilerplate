import { Routes } from '../../interfaces/dashboard';
import NotFoundScreen from '../dashboard/views/screens/NotFoundScreen';
import LoginScreen from './views/screens/loginScreen';

const routes: Array<Routes> = [
    {
        path: 'Root',
        component: LoginScreen
    },
    {
        path: 'NotFound',
        component: NotFoundScreen,
        options: { title: 'Oops!' }
    }
];

export { routes };