import { combineReducers } from 'redux';
import LoginContainer from './login';
import dashboardContainer from './dashboard';


const containers = {
  reducers: combineReducers({
    login: LoginContainer.reducer,
    dashboard: dashboardContainer.reducer,
  }),
  sagas: {
    dashboard: dashboardContainer.saga,
    login: LoginContainer.saga
  },
};


export default containers;
