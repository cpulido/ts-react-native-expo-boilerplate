
export default {
    emit(event: any, data = null) {
      // @ts-ignore
      if (this.dataSource[event]) {
        // @ts-ignore
        const callbacksIDs = Object.keys(this.dataSource[event]);
  
        callbacksIDs.forEach((callbackID) => {
          // @ts-ignore
          const callback = this.dataSource[event][callbackID];
          callback(data);
        });
      }
    },
    on(event: any, callback: Function) {
      const callbackID = Math.random().toString(36).slice(-5);
      // @ts-ignore
      if (!this.dataSource[event]) {
        // @ts-ignore
        this.dataSource[event] = {};
      }
      // @ts-ignore
      this.dataSource[event][callbackID] = callback;
      return callbackID;
    },
    unsubscribe(event: any, callbackID: any) {
      // @ts-ignore
      if (this.dataSource[event] && this.dataSource[event][callbackID]) {
        // @ts-ignore
        delete this.dataSource[event][callbackID];
      }
    },
    dataSource: {},
  };