import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Picker, Label, Icon, View, Button, Spinner } from 'native-base';
import { Platform, Text } from 'react-native';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const { Item } = Picker;

const styles = {
  input: {
    position: 'relative',
    width: '100%',
    backgroundColor: 'transparent',
    borderRadius: 5,
    paddingLeft: Platform.OS === 'android' ? 10 : 0,
    marginLeft: Platform.OS === 'android' ? 0 : -10,
    marginTop: Platform.OS === 'android' ? 0 : 5,
    zIndex: 2,
    height: 50
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  icon: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 10 : 8,
    right: 10,
    zIndex: 3,
    fontSize:17
  },
  spinner: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? -14 : 8,
    right: 10,
    zIndex: 3,
  },
};


class DropdownInput extends Component {
  constructor(props) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    let value = '';
    if (props.defaultValue) {
      value = props.defaultValue;
    } else if (props.options.length && !props.placeholder) {
      value = props.options[0].value;
    }

    this.state = {
      value,
      valid,
      message: props.required ? `El campo ${props.label} es requerido` : '',
      dirty: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }

    if (!isEqual(prevProps.options, this.props.options)) {
      this.resetInput();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(data) {
    if (data !== this.state.value) {
      this.setState({ value: data });

      // validar input
      if (this.props.required && (!data || data.length === 0)) {
        this.setState({ valid: false, message: `El campo ${this.props.label} es requerido` });
      } else {
        this.setState({ valid: true, message: '', dirty: true });
      }
    }
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    return this.state.value;
  }

  getSelectedOptionLabel() {
    const { value } = this.state;
    const { options } = this.props;
    const selectedOption = options.find((option) => option.value === value);

    if (selectedOption) {
      return selectedOption.text;
    }

    return null;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    let value = '';
    if (this.props.defaultValue) {
      value = this.props.defaultValue;
    } else if (this.props.options.length && !this.props.placeholder) {
      value = this.props.options[0].value;
    }

    this.setState({
      value,
      valid,
      message: this.props.required ? `El campo ${this.props.label} es requerido` : '',
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  renderIcon() {
    const { inputStyle, loading, placeholderColor } = this.props;
    const invalidInput = this.state.dirty && !this.state.valid;
    const iconStyle = { ...styles.icon };
    let iconName = 'caret-down';

    if (loading) {
      let color = 'black';

      if (inputStyle && inputStyle.color) {
        color = inputStyle.color;
      }

      if (placeholderColor) {
        color = placeholderColor;
      }

      return <Spinner style={styles.spinner} color={color} />;
    }

    if (placeholderColor) {
      iconStyle.color = placeholderColor;
    } else if (inputStyle && inputStyle.color) {
      iconStyle.color = inputStyle.color;
    } else {
      iconStyle.color = 'black';
    }

    if (invalidInput) {
      iconName = 'md-close-circle';
      iconStyle.color = 'red';
    }

    return <Icon style={iconStyle} name={iconName} />;
  }

  renderInput() {
    let inputStyle = { ...this.props.inputStyle, marginTop: 4 };
    const pickerStyle = { ...styles.input };
    const textStyle = {};

    if (this.state.dirty && !this.state.valid) {
      inputStyle = { ...inputStyle, ...styles.errorBorder };
    }

    if (inputStyle.color) {
      if (this.props.placeholderColor && !this.state.value) {
        textStyle.color = this.props.placeholderColor;
      } else {
        textStyle.color = inputStyle.color;
      }

      if (Platform.OS === 'android') {
        pickerStyle.color = textStyle.color;
      }
    }

    if (inputStyle.fontFamily) {
      textStyle.fontFamily = inputStyle.fontFamily;
    }

    delete inputStyle.fontFamily;
    delete inputStyle.color;

    if (!this.props.readOnly) {
      const options = [...this.props.options];

      // add default option if needed
      if (this.props.placeholder) {
        options.unshift({ text: this.props.placeholder, value: '' });
      }

      // add empty option if needed
      if (!options.length) {
        options.unshift({ text: 'No hay opciones disponibles', value: '' });
      }

      return (
        <View style={{borderWidth: 1, borderColor:'rgb(240,240,240)', borderRadius:7}}>
          <Picker
            supportedOrientations={['portrait', 'landscape']}
            iosHeader={this.props.label}
            mode="dropdown"
            selectedValue={this.state.value}
            onValueChange={this.onChange.bind(this)}
            headerBackButtonText="Atras"
            style={pickerStyle}
            textStyle={textStyle}
          >
            { options.map((option, index) =>
              <Item key={index} label={option.text} value={option.value} />,
            )}
          </Picker>
        </View>
      );
    }

    return (
      <Button full transparent style={{ ...inputStyle, justifyContent: 'flex-start', paddingLeft: 10, height: 50 }}>
        <Text style={textStyle}>{ this.getSelectedOptionLabel() || this.props.placeholder }</Text>
      </Button>
    );
  }

  render() {
    return (
      <View style={{justifyContent:'center'}}>
        <Label style={this.props.labelStyle}>
          { `${this.props.label}:` }
        </Label>

        { this.renderInput() }
        { this.renderIcon() }
      </View>
    );
  }
}


DropdownInput.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  required: PropTypes.bool,
  placeholder: PropTypes.string,
  placeholderColor: PropTypes.string,
  label: PropTypes.string,
  setFormData: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  labelStyle: PropTypes.object,
  loading: PropTypes.bool,
  readOnly: PropTypes.bool,
  inputStyle: PropTypes.object,
  defaultValue: PropTypes.oneOfType(
    [PropTypes.string, PropTypes.number],
  ),
};


export default DropdownInput;
