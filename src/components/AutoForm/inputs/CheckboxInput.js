import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, CheckBox, Text } from 'native-base';
import { Col, Grid } from 'react-native-easy-grid';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const styles = {
  label: {
    marginLeft: 2,
    marginBottom: 15,
  },
  checkbox: {
    marginBottom: 13,
  },
  item: {
    marginTop: 5,
  },
};


class CheckboxInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.defaultValue ? props.defaultValue : false,
      valid: true,
      dirty: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onPress() {
    this.setState({ value: !this.state.value, dirty: true });
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    return this.state.value;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : false,
      valid: true,
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  render() {
    return (
      <View style={styles.item}>
        <Grid>
          <Col size={75}>
            <Text style={{ ...styles.label, ...this.props.labelStyle }}>
              { this.props.label }
            </Text>
          </Col>

          <Col size={25}>
            <CheckBox
              checked={this.state.value}
              onPress={this.onPress.bind(this)}
              disabled={this.props.readOnly}
              style={{ ...styles.checkbox, ...this.props.inputStyle }}
            />
          </Col>
        </Grid>
      </View>
    );
  }
}


CheckboxInput.propTypes = {
  defaultValue: PropTypes.bool,
  setFormData: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  labelStyle: PropTypes.object,
  label: PropTypes.string,
  readOnly: PropTypes.bool,
  inputStyle: PropTypes.object,
};


export default CheckboxInput;
