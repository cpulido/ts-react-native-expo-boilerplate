import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Label, Icon, View, Button, Spinner } from 'native-base';
import { TextInput, Platform, Text } from 'react-native';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const styles = {
  input: {
    fontSize: 14,
    letterSpacing: 0.28,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    paddingLeft: 10,
    height: 50,
    zIndex: 2,
  },
  focusedBorder: {
    borderBottomColor: '#0091ea',
    borderBottomWidth: 2,
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  icon: {
    color: 'red',
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 10 : 8,
    right: 10,
    zIndex: 3,
  },
  spinner: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? -14 : 8,
    right: 10,
    zIndex: 3,
  },
};


class EmailInput extends Component {
  constructor(props) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    this.state = {
      value: props.defaultValue ? props.defaultValue : '',
      valid,
      message: props.required ? `The field ${props.label || props.errorLabel} is required.` : '',
      dirty: false,
      focused: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(incomingText) {
    this.setState({ value: incomingText, dirty: true });

    // validar input
    if (this.props.required && !incomingText) {
      this.setState({
        valid: false,
        message: `The field ${this.props.label || this.props.errorLabel} is required.`,
      });
    } else {
      this.setState({ valid: true, message: '' });
    }
  }

  onBlur() {
    const newState = { valid: true, message: '', focused: false };
    const currentValue = this.state.value.trim();
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line

    // actualizar valor
    newState.value = currentValue;
    newState.dirty = true;

    if (this.props.required && !currentValue) {
      newState.valid = false;
      newState.message = `El campo ${this.props.label || this.props.errorLabel} es requerido`;
    } else if (currentValue && !regex.test(currentValue)) {
      newState.valid = false;
      newState.message = 'The email is not formatted correctly.';
    }

    if (this.props.events && this.props.events.onBlur && newState.valid) {
      this.props.events.onBlur(newState.value);
    }

    this.setState(newState);
    return newState;
  }

  onFocus() {
    this.setState({ focused: true });
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    return this.state.value.trim().toLowerCase();
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : '',
      valid,
      message: this.props.required ? `The field ${this.props.label || this.props.errorLabel} is required.` : '',
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  renderIcon() {
    const { inputStyle, loading, placeholderColor } = this.props;
    const invalidInput = this.state.dirty && !this.state.valid;
    const iconStyle = { ...styles.icon };
    let iconName = '';

    if (loading) {
      let color = 'black';

      if (inputStyle && inputStyle.color) {
        color = inputStyle.color;
      }

      if (placeholderColor) {
        color = placeholderColor;
      }

      return <Spinner style={styles.spinner} color={color} />;
    }

    if (placeholderColor) {
      iconStyle.color = placeholderColor;
    } else if (inputStyle && inputStyle.color) {
      iconStyle.color = inputStyle.color;
    } else {
      iconStyle.color = 'black';
    }

    if (invalidInput) {
      iconName = 'md-close-circle';
      iconStyle.color = 'red';
      return <Icon style={iconStyle} name={iconName} />;
    }

    return null;
  }

  renderInput() {
    let inputStyle = { ...styles.input, ...this.props.inputStyle };

    if (this.state.focused) {
      inputStyle = { ...inputStyle, ...styles.focusedBorder };
    }

    if (this.state.dirty && !this.state.valid) {
      inputStyle = { ...inputStyle, ...styles.errorBorder };
    }

    if (this.props.readOnly) {
      const textStyle = { marginLeft: 10 };

      if (inputStyle.color) {
        textStyle.color = inputStyle.color;
      }

      if (inputStyle.fontFamily) {
        textStyle.fontFamily = inputStyle.fontFamily;
      }

      if (inputStyle.fontSize) {
        textStyle.fontSize = inputStyle.fontSize;
      }

      if (inputStyle.letterSpacing) {
        textStyle.letterSpacing = inputStyle.letterSpacing;
      }

      delete inputStyle.fontFamily;
      delete inputStyle.color;
      delete inputStyle.fontSize;
      delete inputStyle.letterSpacing;

      return (
        <Button full transparent style={{ ...inputStyle, justifyContent: 'flex-start', paddingLeft: 0 }}>
          <Text style={textStyle}>{ this.state.value || this.props.placeholder }</Text>
        </Button>
      );
    }

    return (
      <TextInput
        ref={refInput => { this.input = refInput; }}
        value={this.state.value}
        placeholder={this.props.placeholder}
        onChangeText={this.onChange.bind(this)}
        onBlur={this.onBlur.bind(this)}
        onFocus={this.onFocus.bind(this)}
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
        style={inputStyle}
        maxLength={this.props.max}
        placeholderTextColor={this.props.placeholderColor || inputStyle.color || 'black'}
        underlineColorAndroid="transparent"
        // returnKeyType={this.props.lastInput ? 'done' : 'next'}
        // onSubmitEditing={this.onSubmit.bind(this)}
      />
    );
  }

  renderLabel() {
    const { label } = this.props;

    if (label) {
      return (
        <Label style={this.props.labelStyle}>
          { `${label}:` }
        </Label>
      );
    }

    return null;
  }

  render() {
    return (
      <View>
        { this.renderLabel() }
        { this.renderInput() }
        { this.renderIcon() }
      </View>
    );
  }
}


EmailInput.propTypes = {
  defaultValue: PropTypes.string,
  required: PropTypes.bool,
  label: PropTypes.string,
  setFormData: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  max: PropTypes.number,
  loading: PropTypes.bool,
  events: PropTypes.object,
  labelStyle: PropTypes.object,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  inputStyle: PropTypes.object,
  placeholderColor: PropTypes.string,
};


export default EmailInput;
