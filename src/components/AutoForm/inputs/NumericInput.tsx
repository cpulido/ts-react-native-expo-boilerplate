import * as React from 'react';
import { View } from '../../Themed';
import { Input, Button } from 'react-native-elements';
import { Platform, Text } from 'react-native';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const ASCENDING = 'ascending';
const DESCENDING = 'descending';

const styles = {
  input: {
    fontSize: 14,
    letterSpacing: 0.28,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    paddingLeft: 10,
    height: 50,
    zIndex: 2,
  },
  focusedBorder: {
    // borderBottomColor: '#0091ea',
    // borderBottomWidth: 2,
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  icon: {
    color: 'red',
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 10 : 8,
    right: 10,
    zIndex: 3,
  },
  spinner: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? -14 : 8,
    right: 10,
    zIndex: 3,
  },
};


class NumericInput extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    const existDefaultValue = props.defaultValue !== undefined && props.defaultValue !== null && props.defaultValue !== '';
    let value = '';
    let valid;
    let message;

    if (existDefaultValue) {
      valid = true;
      message = '';
    } else if (props.required && props.min && !existDefaultValue) {
      valid = false;
      message = `El campo ${props.label || props.errorLabel} debe contener al menos ${props.min} dígitos`;
    } else {
      valid = !props.required;
      message = props.required ? `The field ${props.label || props.errorLabel} is required` : '';
    }

    if (existDefaultValue && props.mask) {
      value = this.applyMask(props.defaultValue);
    } else if (existDefaultValue && !props.currency) {
      value = props.defaultValue;
    } else if (existDefaultValue && props.currency) {
      value = this.currencyFormat(props.defaultValue);
    }

    this.state = {
      value,
      valid,
      message,
      dirty: false,
      focused: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    const existDefaultValue = nextProps.defaultValue !== undefined && nextProps.defaultValue !== null && nextProps.defaultValue !== '';

    if (!this.state.dirty && existDefaultValue) {
      let value = nextProps.defaultValue;

      if (nextProps.mask) {
        value = this.applyMask(nextProps.defaultValue);
      } else if (nextProps.currency) {
        const parsedNumber = parseFloat(value);
        value = this.currencyFormat(`${parsedNumber}`);
      }

      this.setState({ value, valid: true });
    }
  }

  shouldComponentUpdate(nextProps: any, nextState: any) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(e) {
    if (this.props.mask && Platform.OS === 'android') {
      this.maskedOnChange(e);
    } else {
      this.normalOnChange(e);
    }
  }

  onBlur() {
    this.setState({ focused: false });

    if (this.props.mask && Platform.OS === 'android') {
      return this.maskedOnBlur();
    }

    return this.normalOnBlur();
  }

  onFocus() {
    this.setState({ focused: true });
  }

  getNumericValue(rawValue: string) {
    let numericValue;
    let validNumber = true;

    // get numeric value
    if (this.props.currency) {
      numericValue = rawValue.replace(/[$]/g, '');
      numericValue = numericValue.replace(/[,]/g, '');
    } else {
      numericValue = rawValue;
    }

    // valid number
    if (numericValue && !parseFloat(numericValue)) {
      validNumber = false;
    }

    if (validNumber) {
      if (this.props.currency) {
        const formattedNumber = this.currencyFormat(numericValue);

        return {
          numericValue,
          value: formattedNumber,
          length: numericValue.length,
          valid: true,
        };
      }

      return {
        numericValue,
        value: numericValue,
        length: rawValue.length,
        valid: true,
      };
    }

    return {
      numericValue,
      value: rawValue,
      length: rawValue.length,
      valid: validNumber,
    };
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    let numericValue = '';

    if (this.state.value === '') {
      return '';
    }

    if (this.props.mask) {
      const valueArray = this.state.value.toString().split('');
      const placeholdersPositions = this.getPlaceholdersPositions();

      placeholdersPositions.forEach((placeholderPosition) => {
        numericValue += valueArray[placeholderPosition];
      });
    } else if (this.props.currency) {
      numericValue = this.state.value.toString().replace(/[$]/g, '');
      numericValue = numericValue.replace(/[,]/g, '');
    } else {
      numericValue = this.state.value;
    }

    return numericValue ? parseFloat(numericValue) : '';
  }

  getPlaceholdersPositions() {
    const { mask } = this.props;
    const maskArray = mask.split('');
    const placeholdersPositions: Array<any> = [];

    maskArray.forEach((character: any, index: any) => {
      if (character === 'x') {
        placeholdersPositions.push(index);
      }
    });

    return placeholdersPositions;
  }

  normalOnChange(incomingText: string) {
    let regex;

    if (this.props.integer) {
      regex = /[^\d]/g;
    } else if (this.props.currency) {
      regex = /[^.,$\d]/g;
    } else {
      regex = /[^.\d]/g;
    }

    if (this.props.required && !incomingText) {
      this.setState({
        valid: false,
        value: '',
        message: `The field ${this.props.label || this.props.errorLabel} is required`,
      });
    } else if (!regex.test(incomingText)) {
      const number = this.getNumericValue(incomingText);

      if (number.valid) {
        const newValue = {
          value: number.value,
          message: '',
          valid: true,
          dirty: true,
        };

        if (this.props.max && number.length <= this.props.max) {
          this.setState(newValue);
        } else if (!this.props.max) {
          this.setState(newValue);
        }
      } else {
        this.setState({
          value: number.value,
          valid: false,
          message: `El campo ${this.props.label || this.props.errorLabel} no tiene un formato valido`,
          dirty: true,
        });
      }
    }
  }

  normalOnBlur() {
    const currentText = this.state.value;
    const number = this.getNumericValue(currentText);
    let newState: any = { valid: true, message: '', dirty: true };

    if (this.props.required && !currentText) {
      newState = {
        valid: false,
        value: '',
        message: `The field ${this.props.label || this.props.errorLabel} is required`,
      };
    } else if (number.numericValue && !parseFloat(number.numericValue)) {
      newState = {
        valid: false,
        message: `El campo ${this.props.label || this.props.errorLabel} no tiene un formato valido`,
      };
    } else if (parseFloat(number.numericValue)) {
      const parsedNumber = parseFloat(number.numericValue);

      if (this.props.min && number.length >= this.props.min) {
        newState = {
          value: this.props.currency ? this.currencyFormat(`${parsedNumber}`) : parsedNumber,
          valid: true,
          message: '',
        };
      } else if (this.props.min && number.length < this.props.min) {
        newState = {
          value: this.props.currency ? this.currencyFormat(`${parsedNumber}`) : parsedNumber,
          valid: false,
          message: `El campo ${this.props.label || this.props.errorLabel} debe contener al menos ${this.props.min} números`,
        };
      } else if (!this.props.min) {
        newState = {
          value: this.props.currency ? this.currencyFormat(`${parsedNumber}`) : parsedNumber,
          valid: true,
          message: '',
        };
      }
    }

    this.setState(newState, () => {
      if (this.props.events && this.props.events.onBlur && newState.valid) {
        this.props.events.onBlur(this.getValue());
      }
    });

    return newState;
  }

  maskedOnChange(incomingText: string) {
    const { value } = this.state;
    const { mask } = this.props;
    const regex = /[^\d]/g;
    const incomingValueArray = incomingText.split('');
    const currentValueArray = value.split('');
    const direction = incomingValueArray.length > currentValueArray.length ? ASCENDING : DESCENDING;
    const lastCharacterIndex = incomingValueArray.length - 1;
    const lastCharacter = incomingValueArray[lastCharacterIndex];
    const isPlaceholder = this.isPlaceholder(lastCharacterIndex);
    const isNumeric = !regex.test(lastCharacter);
    const isFirstCharacter = incomingValueArray.length === 1;
    let validCharacter = true;

    if (isPlaceholder && !isNumeric) {
      validCharacter = false;
    } else if (!isPlaceholder && isFirstCharacter && !isNumeric) {
      validCharacter = false;
    }

    if (direction === DESCENDING) {
      validCharacter = true;
    }

    if (validCharacter) {
      const maskArray = mask.split('');
      const newValueArray: Array<any> = [];
      const placeholdersPositions = this.getPlaceholdersPositions();
      let charactersPlaced = 0;

      if (direction === ASCENDING) {
        const rawValueArray = maskArray.map((character: any, index: number) => {
          if (character === 'x') {
            let characterToPlace;

            if (!charactersPlaced && incomingValueArray.length === 1) {
              charactersPlaced += 1;
              characterToPlace = incomingValueArray[0];
            } else if (incomingValueArray[index]) {
              charactersPlaced += 1;
              characterToPlace = incomingValueArray[index];

              if (regex.test(characterToPlace)) {
                validCharacter = false;
              }
            }

            return characterToPlace;
          } else if (maskArray[index] !== incomingValueArray[index]) {
            incomingValueArray.splice(index, 0, maskArray[index]);
          }

          return character;
        });

        if (charactersPlaced === placeholdersPositions.length) {
          rawValueArray.forEach((character: any) => { newValueArray.push(character); });
        } else {
          const lastIndex = placeholdersPositions[charactersPlaced];

          rawValueArray.forEach((character: any, index: number) => {
            if (index < lastIndex) {
              newValueArray.push(character);
            }
          });
        }
      } else {
        while (incomingValueArray.length) {
          const lastIndex = incomingValueArray.length - 1;

          if (this.isPlaceholder(lastIndex)) {
            break;
          } else {
            incomingValueArray.pop();
          }
        }

        incomingValueArray.forEach((character) => {
          newValueArray.push(character);
        });
      }

      const finalValue = newValueArray.reduce((sum, currentItem) => (
        sum + currentItem
      ), '');

      if (validCharacter) {
        this.setState({ value: finalValue, valid: true, dirty: true });
      }
    }
  }

  maskedOnBlur() {
    const { required } = this.props;
    const { value } = this.state;
    const placeholdersPositions = this.getPlaceholdersPositions();
    const valueArray = value.split('');
    const regex = /[^\d]/g;
    const newState = { valid: true, message: '', dirty: true };
    const validateValue = value || (!value && required);

    if (validateValue) {
      for (let index = 0; index < placeholdersPositions.length; index += 1) {
        const placeholderIndex = placeholdersPositions[index];
        const currentCharacter = valueArray[placeholderIndex];

        if (!currentCharacter) {
          newState.valid = false;
          newState.message = `El campo ${this.props.label || this.props.errorLabel} debe contener ${placeholdersPositions.length} números`;
          break;
        }

        if (regex.test(currentCharacter)) {
          newState.valid = false;
          newState.message = `El campo ${this.props.label || this.props.errorLabel} contiene caracteres no validos`;
          break;
        }
      }
    }

    if (this.props.events && this.props.events.onBlur && newState.valid) {
      this.props.events.onBlur(this.getValue());
    }

    this.setState(newState);
    return newState;
  }

  isPlaceholder(index: any) {
    const placeholdersPositions = this.getPlaceholdersPositions();

    const foundIndex = placeholdersPositions.filter((placeholdersPosition) =>
      placeholdersPosition === index,
    );

    return foundIndex.length > 0;
  }

  currencyFormat(value: any) {
    const arrayNumber = value.toString().split('.');
    const formattedNumber = `$${arrayNumber[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')}`;

    if (arrayNumber.length > 1) {
      return `${formattedNumber}.${arrayNumber[1]}`;
    }

    return formattedNumber;
  }

  applyMask(numericValue: any) {
    const maskArray = this.props.mask.split('');
    const numericValueArray = numericValue.toString().split('');
    let maskedValue = '';
    let nextIndex = 0;

    maskArray.forEach((character: any) => {
      if (character === 'x') {
        maskedValue += numericValueArray[nextIndex];
        nextIndex += 1;
      } else {
        maskedValue += character;
      }
    });

    return maskedValue;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    const existDefaultValue = this.props.defaultValue !== undefined && this.props.defaultValue !== null && this.props.defaultValue !== '';
    let valid;

    if (existDefaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    let value = '';
    if (existDefaultValue && this.props.mask) {
      value = this.applyMask(this.props.defaultValue);
    } else if (existDefaultValue && !this.props.currency) {
      value = this.props.defaultValue;
    } else if (existDefaultValue && this.props.currency) {
      value = this.currencyFormat(this.props.defaultValue);
    }

    this.setState({
      value,
      valid,
      message: this.props.required ? `The field ${this.props.label || this.props.errorLabel} is required.` : '',
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------

  renderInput() {
    const maskedAndroid = this.props.mask && Platform.OS === 'android';
    let inputStyle = { ...styles.input, ...this.props.inputStyle };

    if (this.state.focused) {
      inputStyle = { ...inputStyle, ...styles.focusedBorder };
    }

    if (this.state.dirty && !this.state.valid) {
      inputStyle = { ...inputStyle, ...styles.errorBorder };
    }

    if (this.props.readOnly) {
      const textStyle: any = { marginLeft: 10 };

      if (inputStyle.color) {
        textStyle.color = inputStyle.color;
      }

      if (inputStyle.fontFamily) {
        textStyle.fontFamily = inputStyle.fontFamily;
      }

      if (inputStyle.fontSize) {
        textStyle.fontSize = inputStyle.fontSize;
      }

      if (inputStyle.letterSpacing) {
        textStyle.letterSpacing = inputStyle.letterSpacing;
      }

      delete inputStyle.fontFamily;
      delete inputStyle.color;
      delete inputStyle.fontSize;
      delete inputStyle.letterSpacing;

      return (
        <Button style={{ ...inputStyle, justifyContent: 'flex-start', paddingLeft: 0 }}>
          <Text style={textStyle}>{ this.state.value || this.props.placeholder }</Text>
        </Button>
      );
    }

    return (
      <Input
        // @ts-ignore
        ref={refInput => { this.input = refInput; }}
        value={`${this.state.value}`}
        placeholder={this.props.placeholder}
        onChangeText={this.onChange.bind(this)}
        keyboardType="numeric"
        onBlur={this.onBlur.bind(this)}
        errorMessage={this.props.errorMessage}
        autoCapitalize={this.props.autoCapitalize}
        placeholderTextColor={this.props.placeholderColor || inputStyle.color || 'black'}
        onFocus={this.onFocus.bind(this)}
        style={inputStyle}
        maxLength={!maskedAndroid ? this.props.max : null}
        underlineColorAndroid="transparent"
        leftIcon={this.props.leftIcon}
        inputContainerStyle={{}}
        containerStyle={this.props.containerStyle}
        label={this.props.label}
      />
    );
  }

  // renderLabel() {
  //  const { label } = this.props;
  //
  //  if (label) {
  //    return (
  //      <Label style={this.props.labelStyle}>
  //        { `${this.props.label}:` }
  //      </Label>
  //    );
  //  }
  // }

  render() {
    return (
      <View>
        { this.renderInput() }
      </View>
    );
  }
}



export default NumericInput;
