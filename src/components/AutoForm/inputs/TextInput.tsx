import React, { Component } from 'react';
import { View } from '../../Themed';
// import { Label, Icon, Button, Spinner } from 'native-base';
import { Button, Icon, Input } from 'react-native-elements';

import { Platform, Text, TouchableOpacity } from 'react-native';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const styles = {
  input: {
    fontSize: 14,
    letterSpacing: 0.28,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 5,
    // marginTop: 5,
    paddingLeft: 10,
    height: 50,
    zIndex: 2,
  },
  focusedBorder: {
    // borderBottomColor: '#0091ea',
    // borderBottomWidth: 1,
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  icon: {
    color: 'red',
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 10 : 8,
    right: 10,
    zIndex: 3,
  },
  spinner: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? -14 : 8,
    right: 10,
    zIndex: 3,
  },
};


class TextInputComponent extends Component<any, any> {
  constructor(props: any) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    this.state = {
      value: props.defaultValue ? props.defaultValue : '',
      valid,
      message: props.required ? `The field ${props.label || props.errorLabel} is required` : '',
      dirty: false,
      focused: false,
      seePassword:this.props.password ? true : false
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps: any) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    } else if (this.props.readOnly && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps: any, nextState: any) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(incomingText: any) {
    if (this.props.required && !incomingText) {
      this.setState({
        valid: false,
        dirty: true,
        value: '',
        message: `The field ${this.props.label || this.props.errorLabel} is required`,
      });
    } else if (Platform.OS === 'android') {
      const regex = /[^A-ZÑ a-zñ0-9]/g;
      let matchRegex = false;

      if (this.props.alphanumeric && !regex.test(incomingText)) {
        matchRegex = true;
      } else if (!this.props.alphanumeric) {
        matchRegex = true;
      }

      if (matchRegex && this.props.max && incomingText.length <= this.props.max) {
        this.setValue(incomingText);
      } else if (matchRegex && !this.props.max) {
        this.setValue(incomingText);
      }
    } else if (Platform.OS === 'ios') {
      this.setState({ value: incomingText, dirty: true });
      this.validateText(incomingText, null);
    }
  }

  onBlur() {
    this.setState({ focused: false });
    const currentValue = this.state.value;
    let extraValidation;

    if (this.props.regex) {
      extraValidation = () => {
        let nextState = { valid: true, message: '' };
        const regex = new RegExp(this.props.regex, 'g');

        if (!regex.test(currentValue)) {
          nextState = { valid: false, message: `The field ${this.props.label || this.props.errorLabel} don´t have a valid format.` };
          this.setState(nextState);
          return nextState;
        }

        return nextState;
      };
    }

    const nextState = this.validateText(currentValue, extraValidation);

    if (this.props.events && this.props.events.onBlur && nextState.valid) {
      this.props.events.onBlur(currentValue);
    }

    return nextState;
  }

  onFocus() {
    this.setState({ focused: true });
  }

  getValue() {
    return this.state.value.trim();
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  setValue(value: any) {
    let valueData;
    this.setState({ dirty: true });

    // custom validations
    if (this.props.validator) {
      valueData = this.props.validator(value);

      this.setState({
        value,
        valid: valueData.valid,
        message: valueData.message ? valueData.message : '',
      });
    } else {
      this.setState({ value, valid: true, message: '' });
    }
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  validateText(textToValidate: any, extraValidation: any) {
    let matchRegex = false;
    let nextState = { valid: true, message: '' };
    const regex = /[^A-ZÑ a-zñ0-9]/g;

    if (this.props.alphanumeric && !regex.test(textToValidate)) {
      matchRegex = true;
    } else if (!this.props.alphanumeric) {
      matchRegex = true;
    }

    if (!matchRegex) {
      nextState = { valid: false, message: `El campo ${this.props.label || this.props.errorLabel} solo puede contener caracteres alfanumericos` };
    } else if (this.props.required && !textToValidate) {
      nextState = { valid: false, message: `The field ${this.props.label || this.props.errorLabel} is required` };
    } else if (this.props.max && textToValidate.length > this.props.max) {
      nextState = { valid: false, message: `El campo ${this.props.label || this.props.errorLabel} solo puede contener ${this.props.max} caracteres` };
    } else if (this.props.min && textToValidate.length < this.props.min) {
      nextState = { valid: false, message: `El campo ${this.props.label || this.props.errorLabel} debe contener al menos ${this.props.min} caracteres` };
    } else if (extraValidation) {
      nextState = extraValidation();
    }

    this.setState(nextState);
    return nextState;
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : '',
      valid,
      message: this.props.required ? `The field ${this.props.label || this.props.errorLabel} is required` : '',
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------

  renderInput() {
    const { seePassword } = this.state;

    let inputStyle = { ...styles.input, ...this.props.inputStyle };

    let colorIcon = 'black';

    if (inputStyle && inputStyle.color) {
      colorIcon = inputStyle.color;
    }

    let seePasswordIcon = <Icon style={{ color: colorIcon }} name="eye-off" />;
    let iconSection = null;
    const invalidInput = this.state.dirty && !this.state.valid;

    if (this.state.focused) {
      inputStyle = { ...inputStyle, ...styles.focusedBorder };
    }

    if (this.state.dirty && !this.state.valid) {
      inputStyle = { ...inputStyle, ...styles.errorBorder };
    }

    if (this.props.readOnly) {
      const textStyle: any = { marginLeft: 10 };

      if (inputStyle.color) {
        textStyle.color = inputStyle.color;
      }

      if (inputStyle.fontFamily) {
        textStyle.fontFamily = inputStyle.fontFamily;
      }

      if (inputStyle.fontSize) {
        textStyle.fontSize = inputStyle.fontSize;
      }

      if (inputStyle.letterSpacing) {
        textStyle.letterSpacing = inputStyle.letterSpacing;
      }

      delete inputStyle.fontFamily;
      delete inputStyle.color;
      delete inputStyle.fontSize;
      delete inputStyle.letterSpacing;

      return (
        <Button
          type="outline"
          style={{ ...inputStyle, justifyContent: 'flex-start', paddingLeft: 0 }}
          onPress={() => {}}
        >
          <Text style={textStyle}>{ this.state.value || this.props.placeholder }</Text>
        </Button>
      );
    }



    if(seePassword ===false) {
      seePasswordIcon = <Icon style={{ color: colorIcon }} name="eye" />;
    }

    if(this.props.password){
      iconSection=(
        <TouchableOpacity
          style={{
            position: 'absolute',
            bottom: Platform.OS === 'android' ? 10 : 8,
            right: 10,
            zIndex: 3
          }}
          onPress={() => { this.setState({seePassword:!this.state.seePassword}) }}>
                { seePasswordIcon }
        </TouchableOpacity>
      );
    }
    if(invalidInput){
      iconSection=null;
    }


    return (
      <View>
        <Input
          // @ts-ignore
          ref={refInput => { this.input = refInput; }}
          secureTextEntry={seePassword}
          value={this.state.value}
          
          placeholder={this.props.placeholder}
          onChangeText={this.onChange.bind(this)}
          onBlur={this.onBlur.bind(this)}
          onFocus={this.onFocus.bind(this)}
          style={inputStyle}
          errorMessage={this.props.errorMessage}
          maxLength={this.props.max}
          autoCapitalize={this.props.autoCapitalize}
          placeholderTextColor={this.props.placeholderColor || inputStyle.color || 'black'}
          underlineColorAndroid="transparent"
          leftIcon={this.props.leftIcon}
          inputContainerStyle={{

          }}
          containerStyle={this.props.containerStyle}
          label={this.props.label}
          // returnKeyType={this.props.lastInput ? 'done' : 'next'}
          // onSubmitEditing={this.onSubmit.bind(this)}
        />
        { iconSection }
    </View>
    );
  }

  // renderLabel() {
  //  const { label } = this.props;
  //
  //  if (label) {
  //    return (
  //      <Label style={this.props.labelStyle}>
  //        { `${label}:` }
  //      </Label>
  //    );
  //  }
  //
  //  return null;
  // }

  render() {
    return (
      <View>
        { this.renderInput() }
      </View>
    );
  }
}

export default TextInputComponent;
