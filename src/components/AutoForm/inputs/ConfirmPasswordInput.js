import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Label, Icon } from 'native-base';
import { TextInput, Platform, TouchableOpacity } from 'react-native';
import { isEqual } from 'lodash';
import zxcvbn from 'zxcvbn';
import { Col, Grid } from 'react-native-easy-grid';
import formatter from '../../../utils/formatter';

const styles = {
  input: {
    fontSize: 14,
    letterSpacing: 0.28,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    paddingLeft: 10,
    height: 50,
    zIndex: 2,
    marginBottom:5
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  icon: {
    color: 'red',
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 2 : 7,
    right: 10,
    zIndex: 3,
  },
  Passwordicon: {
    color: 'white',
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 2 : 7,
    right: 10,
    zIndex: 3,
  },
  strength: {
    borderBottomColor: 'rgb(200,200,200)',
    borderBottomWidth: 5,
    transition: 'all 250ms ease'
  }
};

const colors = ['darkred', 'orangered', 'orange', 'yellowgreen', 'green'];

class ConfirmPasswordInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      secondValue: '',
      valid: false,
      message: `The field ${props.label} is required.`,
      dirty: false,
      firstErrorVisible: false,
      secondErrorVisible: false,
      firstStrength: 0,
      secondStrength: 0,
      seePassword: false,
      firstInputFocused: false
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(incomingText) {
    this.setState({ value: incomingText });

    // validar input
    if (!incomingText) {
      this.setState({
        valid: false,
        message: 'The password field is required.',
      });
    } else if (!this.props.max || (this.props.max && incomingText.length <= this.props.max)) {
      const strength = zxcvbn(incomingText).score;
      this.setState({ value: incomingText, firstStrength: strength });

      if (this.props.minStrength && strength < this.props.minStrength) {
        this.setState({
          valid: false,
          firstErrorVisible: true,
          secondErrorVisible: false,
        });
      } else if (incomingText && !this.state.secondValue) {
        this.setState({
          valid: false,
          message: 'Password confirmation missing.',
        });
      } else if (this.state.dirty && incomingText !== this.state.secondValue) {
        this.setState({
          valid: false,
          message: 'Passwords do not match.',
        });
      } else if (incomingText === this.state.secondValue) {
        this.setState({ valid: true, message: '' });
      }
    }
  }

  onChangeSecond(incomingText) {
    this.setState({ secondValue: incomingText, dirty: true });

    // validar inputs
    if (this.state.value !== '' && this.state.value === incomingText) {
      this.setState({ valid: true, message: '' });
    } else {
      this.setState({ valid: false, message: 'Passwords do not match' });
    }
  }

  onFocus(internalIndex, keyboardOffset) {
    this.setState({ focused: true });
    //this.props.onFocus(this.props.index + internalIndex, keyboardOffset);
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    return this.state.value;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    this.setState({
      value: '',
      secondValue: '',
      valid: false,
      message: `The field ${this.props.label} is required`,
      dirty: false,
    });
  }


  renderFirstStrength() {
    const { value, firstStrength } = this.state;
    const passwordLength = value.length;

    const styleLine1 = { ...styles.strength };
    const styleLine2 = { ...styles.strength };
    const styleLine3 = { ...styles.strength };
    const styleLine4 = { ...styles.strength };
    const styleLine5 = { ...styles.strength };

    if (passwordLength > 0) {
      switch (firstStrength) {
        case 0:
          styleLine1.borderBottomColor = `${colors[firstStrength]}`;
          break;
        case 1:
          styleLine1.borderBottomColor = `${colors[firstStrength]}`;
          styleLine2.borderBottomColor = `${colors[firstStrength]}`;
          break;
        case 2:
          styleLine1.borderBottomColor = `${colors[firstStrength]}`;
          styleLine2.borderBottomColor = `${colors[firstStrength]}`;
          styleLine3.borderBottomColor = `${colors[firstStrength]}`;
          break;
        case 3:
          styleLine1.borderBottomColor = `${colors[firstStrength]}`;
          styleLine2.borderBottomColor = `${colors[firstStrength]}`;
          styleLine3.borderBottomColor = `${colors[firstStrength]}`;
          styleLine4.borderBottomColor = `${colors[firstStrength]}`;
          break;
        default:
          styleLine1.borderBottomColor = `${colors[firstStrength]}`;
          styleLine2.borderBottomColor = `${colors[firstStrength]}`;
          styleLine3.borderBottomColor = `${colors[firstStrength]}`;
          styleLine4.borderBottomColor = `${colors[firstStrength]}`;
          styleLine5.borderBottomColor = `${colors[firstStrength]}`;
          break;
      }
    }

    if (this.props.minStrength) {
      return (
        <View>
          <Grid columns={5} >
            <Col style={styleLine1} />
            <Col style={styleLine2} />
            <Col style={styleLine3} />
            <Col style={styleLine4} />
            <Col style={styleLine5} />
          </Grid>
        </View>
      );
    }
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  render() {
    const { seePassword } = this.state;

    let inputStyle = { ...styles.input, ...this.props.inputStyle };
    const invalidInput = this.state.dirty && !this.state.valid;
    let errorIcon;
    let seePasswordIcon = <Icon style={{color:inputStyle.color, padding:5}} name="eye" />;

    if(seePassword ===false){
      seePasswordIcon = <Icon style={{color:inputStyle.color, padding:5}} name="eye-off" />;
    }

    if (invalidInput) {
      errorIcon = <Icon style={styles.icon} name="md-close-circle" />;
      inputStyle = { ...inputStyle, ...styles.errorBorder };
    }

    return (
      <View>
        <View style={{ marginBottom: 15 }}>
          <Label style={this.props.labelStyle}>
            { `${this.props.label}:` }
          </Label>
          <TextInput
            secureTextEntry={!seePassword}
            ref={refInput => { this.input = refInput; }}
            value={this.state.value}
            placeholder={this.props.placeholder}
            onChangeText={this.onChange.bind(this)}
            //onFocus={() => { this.onFocus(0, this.props.keyboardOffset); }}
            //onBlur={this.props.onBlur}
            style={inputStyle}
            maxLength={this.props.max}
            placeholderTextColor={this.props.placeholderColor || inputStyle.color || 'black'}
            underlineColorAndroid="transparent"
            // returnKeyType={this.props.lastInput ? 'done' : 'next'}
            // onSubmitEditing={this.onSubmit.bind(this)}
          />

          <TouchableOpacity
            style={{
              position: 'absolute',
              bottom: Platform.OS === 'android' ? 10 : 8,
              right: 10,
              zIndex: 3
            }}
            onPress={() => { this.setState({seePassword:!this.state.seePassword}) }}>
                  { seePasswordIcon }
          </TouchableOpacity>
        </View>

        <View>
          <Label style={this.props.labelStyle}>
            Confirm password:
          </Label>
          <TextInput
            secureTextEntry={!seePassword}
            value={this.state.secondValue}
            placeholder={this.props.placeholder}
            onChangeText={this.onChangeSecond.bind(this)}
            //onFocus={() => { this.onFocus(1, this.props.keyboardOffset + 50); }}
            //onBlur={this.props.onBlur}
            style={inputStyle}
            placeholderTextColor={this.props.placeholderColor || inputStyle.color || 'black'}
            underlineColorAndroid="transparent"
            // returnKeyType={this.props.lastInput ? 'done' : 'next'}
            // onSubmitEditing={this.onSubmit.bind(this)}
          />

          <TouchableOpacity
           style={{
              position: 'absolute',
              bottom: Platform.OS === 'android' ? 10 : 8,
              right: 10,
              zIndex: 3
            }}
          onPress={() => { this.setState({seePassword:!this.state.seePassword}) }}>
                  { seePasswordIcon }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}


ConfirmPasswordInput.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  setFormData: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  max: PropTypes.number,
  labelStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  placeholderColor: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  index: PropTypes.number,
  keyboardOffset: PropTypes.number,
  minStrength: PropTypes.number
};


export default ConfirmPasswordInput;
