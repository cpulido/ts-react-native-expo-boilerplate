// docs: https://github.com/mmazzarolo/react-native-modal-datetime-picker
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Label, Button, Icon } from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import { Platform, Text } from 'react-native';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const FORMAT_STRING = 'D-MMMM-YYYY';

const styles = {
  input: {
    position: 'relative',
    width: '100%',
    left: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: Platform.OS === 'android' ? 10 : 0,
    marginLeft: 0,
    marginTop: 5,
    height: 50,
    zIndex: 2,
    justifyContent: 'flex-start',
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  icon: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 10 : 8,
    right: 10,
    zIndex: 3,
  },
  text: {
    marginTop: Platform.OS === 'android' ? 0 : 5,
  },
};


class DateInput extends Component {
  constructor(props) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    this.state = {
      value: props.defaultValue ? props.defaultValue : null,
      valid,
      message: props.required ? `El campo ${props.label} es requerido` : '',
      pickerVisible: false,
      dirty: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(newDate) {
    this.setState({ value: newDate, dirty: true, pickerVisible: false });

    // validar input
    if (this.props.required && !newDate) {
      this.setState({ valid: false, message: `El campo ${this.props.label} es requerido` });
    } else {
      this.setState({ valid: true, message: '' });
    }
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    const date = this.state.value;
    return this.props.format ? moment(date).format(this.props.format) : date;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : new Date(),
      valid,
      message: this.props.required ? `El campo ${this.props.label} es requerido` : '',
      dirty: false,
    });
  }

  togglePicker() {
    if (!this.props.readOnly) {
      this.setState({ pickerVisible: !this.state.pickerVisible });
    }
  }


  // -------------------------- 
  // ------ render methods ----
  // --------------------------
  renderIcon() {
    const { inputStyle, placeholderColor } = this.props;
    const invalidInput = this.state.dirty && !this.state.valid;
    const iconStyle = { ...styles.icon };
    let iconName = 'md-calendar';

    if (placeholderColor) {
      iconStyle.color = placeholderColor;
    } else if (inputStyle && inputStyle.color) {
      iconStyle.color = inputStyle.color;
    } else {
      iconStyle.color = 'black';
    }

    if (invalidInput) {
      iconName = 'md-close-circle';
      iconStyle.color = 'red';
    }

    return <Icon style={iconStyle} name={iconName} />;
  }

  render() {
    const textStyle = { ...styles.text };
    let inputStyle = { ...styles.input, ...this.props.inputStyle };
    let placeholder = 'Seleccionar';

    if (this.state.value) {
      placeholder = moment(this.state.value).format(FORMAT_STRING);
    }

    if (this.state.dirty && !this.state.valid) {
      inputStyle = { ...inputStyle, ...styles.errorBorder };
    }

    if (inputStyle.color) {
      textStyle.color = inputStyle.color;
    }

    if (this.props.placeholderColor && !this.state.value) {
      textStyle.color = this.props.placeholderColor;
    }

    if (inputStyle.fontFamily) {
      textStyle.fontFamily = inputStyle.fontFamily;
    }

    delete inputStyle.fontFamily;
    delete inputStyle.color;

    return (
      <View>
        <Label style={this.props.labelStyle}>
          { `${this.props.label}:` }
        </Label>

        <Button
          full
          transparent
          onPress={this.togglePicker.bind(this)}
          style={inputStyle}
        >
          <Text style={textStyle}>
            { placeholder }
          </Text>
        </Button>

        { this.renderIcon() }

        <DateTimePicker
          date={this.state.value || new Date()}
          maximumDate={this.props.max ? this.props.max : undefined}
          minimumDate={this.props.min ? this.props.min : undefined}
          mode={this.props.mode ? this.props.mode : 'date'}
          isVisible={this.state.pickerVisible}
          onConfirm={this.onChange.bind(this)}
          onCancel={this.togglePicker.bind(this)}
        />
      </View>
    );
  }
}


DateInput.propTypes = {
  min: PropTypes.instanceOf(Date),
  max: PropTypes.instanceOf(Date),
  required: PropTypes.bool,
  label: PropTypes.string,
  setFormData: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  format: PropTypes.string,
  labelStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  placeholderColor: PropTypes.string,
  readOnly: PropTypes.bool,
  mode: PropTypes.string,
  defaultValue: PropTypes.oneOfType(
    [PropTypes.instanceOf(Date), PropTypes.string],
  ),
};


export default DateInput;
