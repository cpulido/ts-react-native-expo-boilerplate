import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Label, Icon, View, Button } from 'native-base';
import { TextInput, Platform, Text } from 'react-native';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const styles = {
  input: {
    fontSize: 14,
    letterSpacing: 0.28,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 5,
    paddingLeft: 10,
    height: 50,
    zIndex: 2,
  },
  focusedBorder: {
    borderBottomColor: '#0091ea',
    borderBottomWidth: 2,
  },
  errorBorder: {
    borderBottomColor: 'red',
    borderBottomWidth: 2,
  },
  icon: {
    color: 'red',
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 10 : 8,
    right: 10,
    zIndex: 3,
  },
};


class URLInput extends Component {
  constructor(props) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    this.state = {
      value: props.defaultValue ? props.defaultValue : '',
      valid,
      message: props.required ? `El campo ${props.label} es requerido` : '',
      dirty: false,
      focused: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(incomingText) {
    this.setState({ value: incomingText, dirty: true });

    // validar input
    if (this.props.required && !incomingText) {
      this.setState({
        valid: false,
        message: `El campo ${this.props.label} es requerido`,
      });
    } else {
      this.setState({ valid: true, message: '' });
    }
  }

  onBlur() {
    const newState = { valid: true, message: '', focused: false };
    const currentValue = this.state.value.trim();
    const regex = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/i; // eslint-disable-line

    // actualizar valor
    newState.value = currentValue;
    newState.dirty = true;

    if (this.props.required && !currentValue) {
      newState.valid = false;
      newState.message = `El campo ${this.props.label} es requerido`;
    } else if (currentValue && !regex.test(currentValue)) {
      newState.valid = false;
      newState.message = `El campo ${this.props.label} no tiene un formato correcto`;
    }

    this.setState(newState);
    return newState;
  }

  onFocus() {
    this.setState({ focused: true });
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    return this.state.value.trim();
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : '',
      valid,
      message: this.props.required ? `El campo ${this.props.label} es requerido` : '',
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  renderIcon() {
    const { inputStyle } = this.props;
    const invalidInput = this.state.dirty && !this.state.valid;
    const iconStyle = { ...styles.icon };
    let iconName = 'md-globe';

    if (inputStyle && inputStyle.color) {
      iconStyle.color = inputStyle.color;
    } else if (this.props.placeholderColor) {
      iconStyle.color = this.props.placeholderColor;
    } else {
      iconStyle.color = 'black';
    }

    if (invalidInput) {
      iconName = 'md-close-circle';
      iconStyle.color = 'red';
    }

    return <Icon style={iconStyle} name={iconName} />;
  }

  renderInput() {
    let inputStyle = { ...styles.input, ...this.props.inputStyle };

    if (this.state.focused) {
      inputStyle = { ...inputStyle, ...styles.focusedBorder };
    }

    if (this.state.dirty && !this.state.valid) {
      inputStyle = { ...inputStyle, ...styles.errorBorder };
    }

    if (this.props.readOnly) {
      const textStyle = { marginLeft: 10 };

      if (inputStyle.color) {
        textStyle.color = inputStyle.color;
      }

      if (inputStyle.fontFamily) {
        textStyle.fontFamily = inputStyle.fontFamily;
      }

      if (inputStyle.fontSize) {
        textStyle.fontSize = inputStyle.fontSize;
      }

      if (inputStyle.letterSpacing) {
        textStyle.letterSpacing = inputStyle.letterSpacing;
      }

      delete inputStyle.fontFamily;
      delete inputStyle.color;
      delete inputStyle.fontSize;
      delete inputStyle.letterSpacing;

      return (
        <Button full transparent style={{ ...inputStyle, justifyContent: 'flex-start', paddingLeft: 0 }}>
          <Text style={textStyle}>{ this.state.value || this.props.placeholder }</Text>
        </Button>
      );
    }

    return (
      <TextInput
        ref={refInput => { this.input = refInput; }}
        value={this.state.value}
        placeholder={this.props.placeholder}
        onChangeText={this.onChange.bind(this)}
        onBlur={this.onBlur.bind(this)}
        onFocus={this.onFocus.bind(this)}
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
        style={inputStyle}
        placeholderTextColor={this.props.placeholderColor || inputStyle.color || 'black'}
        underlineColorAndroid="transparent"
        // returnKeyType={this.props.lastInput ? 'done' : 'next'}
        // onSubmitEditing={this.onSubmit.bind(this)}
      />
    );
  }

  render() {
    return (
      <View>
        <Label style={this.props.labelStyle}>
          { `${this.props.label}:` }
        </Label>

        { this.renderInput() }
        { this.renderIcon() }
      </View>
    );
  }
}


URLInput.propTypes = {
  defaultValue: PropTypes.string,
  required: PropTypes.bool,
  label: PropTypes.string,
  setFormData: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  labelStyle: PropTypes.object,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  inputStyle: PropTypes.object,
  placeholderColor: PropTypes.string,
};


export default URLInput;
