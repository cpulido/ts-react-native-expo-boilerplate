import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Label, Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { isEqual } from 'lodash';
import formatter from '../../../utils/formatter';

const styles = {
  label: {
    marginLeft: 2,
    marginBottom: 15,
  },
  item: {
    marginTop: 5,
  },
  radioInput: {
    color: '#0091ea',
  },
  radioBtnText: {
    marginLeft: 5,
    fontSize: 16,
    letterSpacing: 0.32,
    color: 'black',
  },
};


class RadioInput extends Component {
  constructor(props) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    this.state = {
      value: props.defaultValue || '',
      valid,
      message: props.required ? `El campo ${props.label || props.errorLabel} es requerido` : '',
      dirty: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!isEqual(prevState, this.state)) {
      this.setForm();
    }

    if (!isEqual(prevProps.options, this.props.options)) {
      this.resetInput();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(value) {
    this.setState({ value, dirty: true, valid: true });
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    return this.state.value;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : '',
      valid,
      message: this.props.required ? `El campo ${this.props.label || this.props.errorLabel} es requerido` : '',
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  renderRadios() {
    const { options, direction } = this.props;
    const textStyle = { ...styles.radioBtnText };
    const inputStyle = { ...this.props.inputStyle };

    if (inputStyle.color) {
      textStyle.color = inputStyle.color;
    }

    if (inputStyle.fontFamily) {
      textStyle.fontFamily = inputStyle.fontFamily;
    }

    if (inputStyle.fontSize) {
      textStyle.fontSize = inputStyle.fontSize;
    }

    if (inputStyle.letterSpacing) {
      textStyle.letterSpacing = inputStyle.letterSpacing;
    }

    delete inputStyle.fontFamily;
    delete inputStyle.color;
    delete inputStyle.fontSize;
    delete inputStyle.letterSpacing;

    const radios = options.map((option) => (
      <Col key={option.value} size={direction === 'row' ? 50 : 100}>
        <TouchableOpacity onPress={() => { this.onChange(option.value); }} style={inputStyle}>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
            <Icon
              name={this.state.value === option.value ? 'md-radio-button-on' : 'md-radio-button-off'}
              style={styles.radioInput}
            />

            <Text style={textStyle}>{ option.text }</Text>
          </View>
        </TouchableOpacity>
      </Col>
    ));

    const rows = [];
    let currentRadios = [];
    const maxColumnsInRow = direction === 'column' ? 1 : 2;

    radios.forEach((radio, index) => {
      if (currentRadios.length < maxColumnsInRow) {
        currentRadios.push(radio);
      }

      if (currentRadios.length === maxColumnsInRow) {
        rows.push((
          <Row key={`${index}-${currentRadios.length}`}>
            { currentRadios }
          </Row>
        ));

        currentRadios = [];
      }
    });

    return rows;
  }

  renderLabel() {
    const { label } = this.props;

    if (label) {
      return (
        <Label style={this.props.labelStyle}>
          { `${this.props.label}:` }
        </Label>
      );
    }

    return null;
  }

  render() {
    return (
      <View style={styles.item}>
        { this.renderLabel() }

        <Grid>
          { this.renderRadios() }
        </Grid>
      </View>
    );
  }
}


RadioInput.propTypes = {
  defaultValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  setFormData: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  labelStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  label: PropTypes.string,
  errorLabel: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  required: PropTypes.bool,
  direction: PropTypes.oneOf(['row', 'column']).isRequired,
};


export default RadioInput;
