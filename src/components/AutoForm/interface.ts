export interface Props {
    inputs: Array<Input>,
    onChange?: Function,
    onLayout?: any,
    onError?: Function,
    style?: any,
  }
  
  export interface State {
    formData: any,
    rows: Array<any>
  }

  export interface Input {
      name: string,
      type: string,
      placeholder?: string,
      errorMessage?: string,
      defaultValue?: any,
      value?: any,
      max?: number,
      label?: string,
      secureTextEntry?: boolean,
      password?: boolean,
      required?: boolean,
      readOnly?: boolean,
      alphanumeric?: boolean,
      regex?: string,
      leftIcon?: Object,
  }
  