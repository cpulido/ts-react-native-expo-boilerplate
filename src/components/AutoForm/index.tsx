import * as React from 'react';
import { View } from 'native-base';

import { isEqual } from 'lodash';
import formatter from '../../utils/formatter';
import TextInput from './inputs/TextInput';
import NumericInput from './inputs/NumericInput';
import EmailInput from './inputs/EmailInput';
import DropdownInput from './inputs/DropdownInput';
import ConfirmPasswordInput from './inputs/ConfirmPasswordInput';
import CheckboxInput from './inputs/CheckboxInput';
import RadioInput from './inputs/RadioInput';
// import DateInput from './inputs/DateInput';
import URLInput from './inputs/URLInput';

import { Props, State } from './interface';

const styles = {
  container: {
    position: 'relative',
    width: '100%',
  },
  row: {
    marginTop: 10,
    marginBottom: 10,
  },
};


class AutoForm extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    // @ts-ignore
    this.formInputs= {}; // form's registered inputs

    this.state = {
      formData: {},
      rows: []
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.getInputsLayout();
  }

  shouldComponentUpdate(nextProps: any, nextState: any) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    if (this.props.onChange && !isEqual(prevState.formData, this.state.formData)) {
      this.props.onChange(this.formatFormData());
    }

    if (!isEqual(prevProps.inputs, this.props.inputs)) {
      this.getInputsLayout();
    }
  }


  // --------------------
  // --- form methods ---
  // --------------------
  onLayout(event: any) {
    if (this.props.onLayout) {
      this.props.onLayout({ ...event.nativeEvent.layout });
    }
  }

  setFormData(inputName: any, data: any) {
    const formData = { ...this.state.formData };
    formData[inputName] = data;
    this.setState({ formData });
  }

  getInputsLayout() {
    const { inputs } = this.props;
    const rows: Array<any> = [];
    let rowInProgress = { components: [], width: 0 };

    inputs.forEach((input: any, index: number) => {
      const inputWidth = input.width || 100;
      const nextWidth = rowInProgress.width + inputWidth;

      if (nextWidth < 101) {
        // @ts-ignore
        rowInProgress.components.push(input);
        rowInProgress.width += inputWidth;
      } else {
        rows.push({
          components: rowInProgress.components,
          width: rowInProgress.width
        });

        rowInProgress = { components: [], width: 0 };
        // @ts-ignore
        rowInProgress.components.push(input);
        rowInProgress.width += inputWidth;
      }

      if (index === inputs.length - 1) {
        rows.push({
          components: rowInProgress.components,
          width: rowInProgress.width
        });
      }
    });

    this.setState({ rows });
  }

  getData() {
    this.dirtFormInputs();
    const validForm = this.validForm();

    if (validForm.valid) {
      return { isValid: true, data: this.formatFormData() };
    }

    return { isValid: false, message: validForm.message };
  }

  validForm() {
    const result = { valid: true, message: '' };

    // iterate over inputs
    // @ts-ignore
    Object.keys(this.formInputs).forEach(input => {
      // @ts-ignore
      if (this.formInputs[input]) {
        // @ts-ignore
        const currentInput = this.formInputs[input];

        if (result.valid) {
          if (!currentInput.state.valid) {
            result.valid = false;
            result.message = currentInput.state.message;

            if (this.props.onError) {
              this.props.onError(currentInput.state.message);
            }
          } else if (currentInput.onBlur) {
            const nextState = currentInput.onBlur();

            if (nextState && !nextState.valid) {
              result.valid = false;
              result.message = nextState.message;

              if (this.props.onError) {
                this.props.onError(nextState.message);
              }
            }
          }
        }
      }
    });

    return result;
  }

  formatFormData() {
    const formattedData: any = {};

    // iterate over inputs
    // @ts-ignore
    Object.keys(this.formInputs).forEach(input => {
      // @ts-ignore
      const currentInput = this.formInputs[input];

      if (currentInput !== null) {
        formattedData[input] = currentInput.getValue();
      }
    });

    return formattedData;
  }

  dirtFormInputs() {
    // @ts-ignore
    Object.keys(this.formInputs).forEach((input) => {
      // @ts-ignore
      if (this.formInputs[input]) {
        // @ts-ignore
        this.formInputs[input].dirtInput();
      }
    });
  }

  reset() {
    // @ts-ignore
    Object.keys(this.formInputs).forEach((input) => {
      // @ts-ignore
      if (this.formInputs[input]) {
        // @ts-ignore
        this.formInputs[input].resetInput();
      }
    });
  }


  // ----------------------
  // --- render methods ---
  // ----------------------
  renderInputs() {
    const { rows } = this.state;

    return rows.map((row: any, rowsInput: number) => {
      const inputsRow = row.components.map((inputSettings: any, inputIndex: number) => {
        let inputComponent;

        if (inputSettings.active !== undefined && inputSettings.active === false) {
          return null;
        }

        switch (inputSettings.type) {
          case 'text':
            inputComponent = (
              <TextInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
          case 'number':
            inputComponent = (
              <NumericInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
          case 'email':
            inputComponent = (
              <EmailInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
          /*
          case 'url':
            inputComponent = (
              <URLInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
          case 'dropdown':
            inputComponent = (
              <DropdownInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
          case 'confirmPassword':
            inputComponent = (
              <ConfirmPasswordInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
          case 'checkbox':
            inputComponent = (
              <CheckboxInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
          case 'radio':
            inputComponent = (
              <RadioInput
                {...inputSettings}
                setFormData={this.setFormData.bind(this)}
                // @ts-ignore
                ref={refInput => { this.formInputs[inputSettings.name] = refInput; }}
              />
            );
            break;
            */
          case 'container':
            inputComponent = inputSettings.content;
            break;
          default:
            throw new Error(`Tipo de input "${inputSettings.type}" no soportado por Autoform. Inputs disponibles: "text", "number", "email", "url", "dropdown", "confirmPassword", "checkbox", "radio", "date", "container"`);
        }

        return (
          <View key={inputSettings.name || inputIndex} style={{ ...styles.row, flex: inputSettings.width || 1 }}>
            { inputComponent }
          </View>
        );
      });

      return (
        <View key={rowsInput} style={{ flexDirection: 'row' }}>
          { inputsRow }
        </View>
      );
    });
  }

  render() {
    return (
      <View onLayout={this.onLayout.bind(this)}>
        <View style={{ ...this.props.style, ...styles.container }}>
          { this.renderInputs() }
        </View>
      </View>
    );
  }
}

export default AutoForm;
