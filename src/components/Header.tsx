import * as React from 'react';
import { Header } from 'react-native-elements';
import { Icon } from 'react-native-elements'
import { Text, View } from './Themed';


const styles = {
    containerLogin: {
        borderBottomWidth: 1,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        borderColor: '#dee3e0',
        shadowOpacity: 0.16,
        elevation: 8,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 8,
        paddingBottom: 8
    },
    buttonContainer: {
        flexDirection: 'row',
    },
    card: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 2,
        shadowOpacity: 0.16,
        elevation: 8,
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 5
    }
}  

export default class HeaderComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {};
  }

  componentDidMount() {

  }


  render() {
    return (
        <View style={styles.containerLogin}>
            <Header
                statusBarProps={{ barStyle: 'light-content' }}
                leftComponent={this.props.leftContainer}
                rightComponent={this.props.rightContainer}
                style={{ backgroundColor: 'white' }}
                centerComponent={{ text: this.props.title, style: { color: '#000' } }}
                containerStyle={{
                    backgroundColor: '#FFF',
                    justifyContent: 'space-around',
                }}
            />
        </View>
    );
  }
}