import * as React from 'react';
import { View } from './Themed';

const styles = {
    card: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 2,
        shadowOpacity: 0.16,
        elevation: 8,
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5
    },
    cardWithoutShadowBorders: {
      borderWidth: 1,
      borderRadius: 5,
      borderColor: '#dee3e0',
      padding: 20
    },
}

export default class CardView extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {};
  }

  componentDidMount() {

  }


  render() {
    const propStyle = this.props.style || {};
    const card = this.props.withoutShadowBorder ? styles.cardWithoutShadowBorders : styles.card; 
    return (
      <View style={{ ...propStyle, ...card }}>
          { this.props.children }
      </View>
    );
  }
}