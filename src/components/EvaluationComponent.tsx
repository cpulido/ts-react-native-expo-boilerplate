import React from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import { View } from './Themed';

const styles = StyleSheet.create({
  card: {
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#f5f5f5',
    borderWidth: 1,
    borderRadius: 10,
    paddingBottom: 10,
    paddingTop: 10 
  },
  logo: {
    width: 90,
    height: 75,
    resizeMode: 'stretch',
  },
});

const emotes: any = {
    smile:  'https://firebasestorage.googleapis.com/v0/b/valeamigo-app.appspot.com/o/smile.PNG?alt=media&token=d903627e-943a-48c5-9a08-df4c5725e12e',
    neutral: 'https://firebasestorage.googleapis.com/v0/b/valeamigo-app.appspot.com/o/neutralSmile.PNG?alt=media&token=33e49b36-391d-4c41-b156-1edda042ca2f',
    angry: 'https://firebasestorage.googleapis.com/v0/b/valeamigo-app.appspot.com/o/notSmile.PNG?alt=media&token=28c8c7ab-b8ed-4585-851a-ad598654b942' 
}

export default class EvaluationComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {};
  }
  
  callbackEvaluation() {
    this.props.evaluationEvent(this.props.evaluation);
  }

  renderEmote() {
      const currentEmote = this.props.source;

      if (this.props.emote) {
        return (<Image style={styles.logo} source={{ uri: emotes[this.props.emote] }} />)
      }

      if (this.props.source) {
        return (<Image style={styles.logo} source={{ uri: this.props.source }} />)
      }

      if (!currentEmote || !this.props.emote) {
        return (<Image style={styles.logo} source={{ uri: 'https://firebasestorage.googleapis.com/v0/b/valeamigo-app.appspot.com/o/smile.PNG?alt=media&token=d903627e-943a-48c5-9a08-df4c5725e12e' }} />)
      }
  }

  renderEvaluationUp() {
    return (
        <View style={styles.card}>
            <TouchableOpacity
                style={{ width: '100%', alignItems: 'center' }}
                onPress={this.callbackEvaluation.bind(this)}
            >
              {this.renderEmote()}
            </TouchableOpacity>
        </View>
    );
  }

  render() {
    return (
      <View>
          {this.renderEvaluationUp()}
      </View>
    );
  }
}