export interface request_info {
    success: boolean,
    demo: boolean
  }
  
export interface responseTestData {
  request_info: request_info,
  search_metadata: object,
  inline_tweets: Array<object>
}
  
export interface httpBodyResponse {
  config: object,
  data: responseTestData,
  headers: any
}

export interface Routes {
  path: any,
  component: any,
  options?: any
}