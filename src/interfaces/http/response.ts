interface headers {
    Accept: string,
    'Content-Type': string
}

interface request_info {
    success: boolean,
    demo: boolean
  }
  
  
interface config {
   headers: headers,
   url: string
}

interface responseTestData {
    request_info: request_info,
    search_metadata: object,
    inline_tweets: Array<object>
}

export interface httpBodyResponse {
    config: config,
    data: responseTestData,
    headers: headers,
    status: number,
    statusText: string
}
