# ts-react-native-expo-boilerplate

Expo React Native Boilerplate.
 
features:
 - a simple template with react navigation.
 - use "react redux" for store your data
 - use effect "redux saga" for consume http services
 - use "immer" store for more easy management of data reducers